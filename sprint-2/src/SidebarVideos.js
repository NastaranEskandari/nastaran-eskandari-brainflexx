import React, { Component } from 'react';
import VideoRender from './VideoRender';
import {Link} from 'react-router-dom';
import './App.css';

const baseUrl="https://project-2-api.herokuapp.com";
const endpoint="/videos";
const querystring="?api_key=";
const api_key="82940adb-19ea-45c4-95d1-8c060cfdc2b1";

class SidebarVideos extends Component {
  
  state={
    nextVids:[],
    showmainVideo:{comments: [{}]}
  }
// fetch for the first time page loads
  componentDidMount() {
    fetch(baseUrl + endpoint + querystring + api_key)
    .then(response => response.json())
    .then(data => {
      console.log(this.state);
      this.setState({
        nextVids: data,
    });
      console.log(this.state);
    })
    .catch(err=>{
      console.log(err)
    });
  }

  render(){
    let videos = this.state.nextVids;
    return (
      <div className="main__videoBar--side">
        <p className="Up-nets-text">Up next</p>
        { videos.map(video => {
          return <Link to={"/videos/"+video.id} key={video.id}>
          <VideoRender title={video.title}
                  image={video.image}
                  channel={video.channel}
                  duration={video.duration}
                  views={video.views}
                  
            /></Link>
          })
        }
      </div>
    )
  }  
} 

export default SidebarVideos;