import React, { Component } from 'react';
import Comments from './Comments';
import './App.css';

const baseUrl="https://project-2-api.herokuapp.com";
const endpoint="/videos";
const querystring="?api_key=";
const api_key="82940adb-19ea-45c4-95d1-8c060cfdc2b1";

class MainVideo extends Component {

  state={
    nextVids:[],
    showmainVideo:{comments: [{}]}
  }


  componentDidMount() {
    const getId=this.props.id;
    this.show(getId);
  }
// fetch to show mainVideo
  show = (vidId) =>{
    fetch(baseUrl + endpoint +"/"+vidId+ querystring + api_key)
      .then(res=>res.json())
      .then(data2=>{
        this.setState(
          {
            showmainVideo:data2,
          }
        )
      })
      .catch(err=>{
        console.log(err)
      })
  }
// update when we click on each video
  componentDidUpdate(prevProps, prevState)
  {
    console.log('hi');
    if (this.props.id !== prevProps.id) {
      this.show(this.props.id); 
    }
  }
  
  render() {
    
    return (
      <div className="main__video--side">
        <video controls src={this.state.showmainVideo.video+"?api_key=82940adb-19ea-45c4-95d1-8c060cfdc2b1"}
         poster={this.state.showmainVideo.image} ></video>
        <div className="video__info">
          <p  className="video__caption">
            {this.state.showmainVideo.title}
          </p>
          <div className="video__details">
            <span className="view">{this.state.showmainVideo.views+' views'}</span>
            <a href="#"><span><img src='/Assets/Images/Icons/Thumbs Up.svg' alt="thumbsup" />{this.state.showmainVideo.thumbsUp}</span></a>
            <a  href="#"><span><img src='/Assets/Images/Icons/Thumbs Down.svg' alt="thumbsDown" />{this.state.showmainVideo.thumbsDown}</span></a>
            <a  href="#"><span><img src='/Assets/Images/Icons/Share.svg' alt="Share" />share</span></a>
          </div>
        </div>
        <div className="comments">
          <div className="commenter">
            <img className="commenter__img" src={this.state.showmainVideo.image}/>
            <div className="commenter__info">
              <p className="commenter__info--league">{this.state.showmainVideo.channel}</p>
            </div>
            <button className="subscribe">SUBSCRIBE<span> {this.state.showmainVideo.subscriberCount}</span> </button>
          </div>
          <p className="commenter__info--publishDate">{this.state.showmainVideo.description}</p>
          
        </div>
        <Comments comments={this.state.showmainVideo.comments}/>
        <a className="showmore" href="#" >SHOW MORE</a>
      </div>

    );
  }
}

export default MainVideo;