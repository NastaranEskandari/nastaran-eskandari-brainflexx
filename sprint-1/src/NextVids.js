const nextVids = [
    {
        id: "01",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/920x920.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "02",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/american-league-wild-card-game---minnesota-twins-v-new-york-yankees-8119099224ebf5b5.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "03",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/BASEBALL-MLB-HOU-LAD-.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "04",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/Big-Read-Vladimir-Guerrero-Jr-Swings-470x264.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "05",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/donaldson.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "06",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/hqdefault.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "07",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/THKMOYWFLWJCPXQ.20170430201114.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "08",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/PR6AGOQ7XREI5B7UMKM3KAGWFA.jpg",
        league: "MLB",
        views: "1.1M"
    },
    {
        id: "09",
        title: "TEX@TOR Gm5:Blue Jays take ead in wild 7th inning",
        vidSrc: "./Assets/Images/r241851_600x400_3-2.jpg",
        league: "MLB",
        views: "1.1M"
    }

];
export default nextVids