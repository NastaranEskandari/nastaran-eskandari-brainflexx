import React, { Component } from 'react';

import './App.css';

class LeftSide extends Component {
  render() {

    return (
      <div className="main__left--side">
        <video controls
          src='./Assets/videos/BrainStation Sample Video.mp4' ></video>
        <div className="video__info">
          <p className="video__caption">
            {this.props.videoCaption}
          </p>
          <div className="video__details">
            <span className="view">2,304,189 views</span>
            <a href="#"><span><img src='./Assets/Images/Icons/Thumbs Up.svg' alt="thumbsup" />6.9k</span></a>
            <a href="#"><span><img src='./Assets/Images/Icons/Thumbs Down.svg' alt="thumbsDown" />202</span></a>
            <a href="#"><span><img src='./Assets/Images/Icons/Share.svg' alt="Share" />share</span></a>
          </div>
        </div>
        <div className="comments">
          <div className="commenter">
            <div className="commenter__img"></div>
            <div className="commenter__info">
              <p className="commenter__info--league">MLB</p>
              <p className="commenter__info--publishDate">Published on Oct 14, 2015</p>
            </div>
            <button className="subscribe">SUBSCRIBE<span> 1.1M</span> </button>
          </div>
          <div className="comment-wrapper">
            <p className="comment">10/14/15: Jose Bautista crushes a long go-ahead three-run homer in the 7th inning of ALDS Game 5</p>
            <a className="showmore" href="#" >SHOW MORE</a>
          </div>
        </div>
      </div>

    );
  }
}

export default LeftSide;