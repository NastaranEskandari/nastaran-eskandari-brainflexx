import React, { Component } from 'react';
import Header from './Header';
import nextVids from './NextVids';
import VideoRender from './VideoRender';
import Main from './Main';
import LeftSide from './LeftSide';
import RightSide from './RightSide';
import './App.css';

class App extends Component {

  state={
    nextVids:nextVids
  }
  render() {
    return (
      <div>
        <Header/>
        <div className="main">
        <LeftSide videoCaption="Jose Bautista hammers go-ahead three-run shot in ALDS Game 5, delivers epic bat flip"/>
        <RightSide vidsArray={this.state.nextVids}/>
        </div>
       

      </div>
    );
  }
}

export default App;
