import React, { Component } from 'react';
import LeftSide from './LeftSide';
import RightSide from './RightSide';
import './App.css';

class Main extends Component {
  render() {
    
    return (
      <div className="main">
            <LeftSide videoCaption="Jose Bautista hammers go-ahead three-run shot in ALDS Game 5, delivers epic bat flip"/>
            <RightSide ></RightSide>
      </div>
    );
  }
}

export default Main;