import React, { Component } from 'react';

class VideoRender extends Component{
    render(){
        return(
          
            <div className="displayNextVids"> 
                <a href="#"><img className="NextVidImg" src={this.props.vidSrc}/></a>
                <div className="Nextvid-info">
                <a href="#"><p className="Nextvid-title">{this.props.title}</p></a>
                <a href="#"><p className="NextVidCategory">{this.props.league}</p></a>
                    <p className="NextVidVeiws">{this.props.views + ' '+'views'}</p>
                </div>
            </div>
        )
    }
} 

export default VideoRender