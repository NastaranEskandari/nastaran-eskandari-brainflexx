import React, { Component } from 'react';
import VideoRender from './VideoRender';

import './App.css';
import nextVids from './NextVids';

class RightSide extends Component {
    
  render(){
    let videos = this.props.vidsArray;
    return (
      <div className="main__right--side">
        <p className="Up-nets-text">Up next</p>
        { videos.map(video => {
          return <VideoRender title={video.title}
                  vidSrc={video.vidSrc}
                  league={video.league}
                  views={video.views}
            />
          })
        }
      </div>
    )
  }  
} 

export default RightSide;